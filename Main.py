# -*- coding:utf-8 -*-
"""
@Author: Fei Xue
@E-Mail: FeiXue@nuaa.edu.cn
@File: Main.py
@Time: 2020/7/8 14:34
@Introduction: 
"""


from selenium import webdriver
from selenium.webdriver.firefox.options import Options
import selenium.webdriver.support.ui as ui
import time
import os
from datetime import datetime


def tl_war1208l(browser=None):
    assert browser

    # 打开路由器登录网页
    browser.get("http://192.168.1.1/webpages/login.html")
    # 设置等待时间
    wait = ui.WebDriverWait(browser, 5)

    # ---- 用户登录 ---- #

    # 等待登录页面加载完毕
    wait.until(lambda driver: driver.find_element_by_id('login-btn'))
    # 输入用户名
    name = browser.find_element_by_id('username')
    name.click()
    name.send_keys('admin')
    # 输入密码
    passwd = browser.find_element_by_css_selector("input[class^='text-text password-text']")
    # passwd.click()
    passwd.send_keys("123456")
    # 点击登录
    browser.find_element_by_id('login-btn').click()

    # ---- 界面查找 ---- #

    # 等待路由器设置界面加载完毕
    time.sleep(5)
    wait.until(lambda driver: driver.find_element_by_id('hardware-info'))
    # 读取wan1元素
    wan1 = browser.find_element_by_id("wan1")
    # 在wan1元素上查找IP
    ip = wan1.find_element_by_css_selector("h3[class='ipaddr']")
    # 从IP元素读取内容
    ip_string = ip.get_property('innerText')

    return ip_string


def tl_wdr5620(browser=None):
    assert browser

    # 打开路由器登录网页
    browser.get("http://192.168.1.1")
    # 设置等待时间
    wait = ui.WebDriverWait(browser, 5)

    # ---- 用户登录 ---- #

    # 等待登录页面加载完毕
    wait.until(lambda driver: driver.find_element_by_id('loginSub'))
    # 输入密码
    browser.find_element_by_id('lgPwd').send_keys('12345678')
    # 点击登录
    browser.find_element_by_id('loginSub').click()

    # ---- 界面查找 ---- #

    # 等待路由器设置界面加载完毕
    time.sleep(2)
    wait.until(lambda driver: driver.find_element_by_id('QRCode'))
    # 打开路由器设置
    browser.find_element_by_id("routerSetMbtn").click()
    # 等待设置页面加载完毕
    wait.until(lambda driver: driver.find_element_by_id('sysLog_rsMenu'))
    # 打开上网设置
    browser.find_element_by_id("network_rsMenu").click()
    # 等待页面加载完毕
    wait.until(lambda driver: driver.find_element_by_id('onlineTimeUl'))
    # 读取IP元素
    ip = browser.find_element_by_id("wanIpLbl")
    # 从IP元素读取内容
    ip_string = ip.get_property('innerText')

    return ip_string


def ReadRouterIP():

    # 设置隐藏界面参数
    options = Options()
    options.add_argument('--headless')

    # 打开浏览器
    browser = webdriver.Firefox(options=options)

    # 路由器的IP读取函数
    # ip = tl_war1208l(browser)
    ip = tl_wdr5620(browser)

    # 关闭和退出浏览器
    browser.close()
    browser.quit()

    # 读取上次IP，如果相同则不上传
    ip_line = ''
    with open('Readme.md', 'r') as f:
        lines = f.readlines()
        # 按行读取找到第一行IP
        for i in lines:
            if 'FTP' in i:
                ip_line = i
                break
    assert ip_line
    # 从行中截取IP
    _, last_ip = ip_line.split('：')
    # 去除IP字符串的空格
    last_ip = last_ip.strip()

    if ip == last_ip:
        print(f'IP unchanged: {last_ip}')
        return

    # 将IP地址写入文件
    t = datetime.now().strftime("%Y-%m-%d %H:%M:%S")
    with open("Readme.md", "w") as f:
        w_string = "## 读取路由器的 WAN 口 IP 地址  \n"
        w_string += "利用 webdriver 模拟登录路由器管理界面，并通过 find_element 函数获取 IP 地址。  \n"
        w_string += f"## 服务器当前 IP （更新时间 {t}）  \n"
        w_string += f"* 四卡服务器远程桌面连接和 sFTP 连接： {ip}  \n"
        w_string += f"* NAS文件服务器 sFTP 连接： {ip}:2222  \n"
        f.write(w_string)

    # git推送
    os.system('git add Readme.md')
    cmd = 'git commit -m "update at {}"'.format(t)
    os.system(cmd)
    os.system('git push')
    print(f"{t} IP update: {ip}\n")


if __name__ == "__main__":
    ReadRouterIP()

